from django.contrib import admin

# Register your models here.

from .models import *


class MobileImageLinkAdmin(admin.ModelAdmin):
	list_display = ('image_link',)
	search_fields = ('image_link',)


class MobileSpecificationAdmin(admin.ModelAdmin):
	list_display = ('key_name', 'value_info')
	search_fields = ('key_name', 'value_info')


class MobileInfoAdmin(admin.ModelAdmin):
    list_display = ('domain', 'datatype', 'brand', 'model_name', 'model_id', 'title', 
    	'sub_title', 'price', 'rating', 'buy_link', 'added', 'updated')
    search_fields = ('title', 'sub_title')

class MobileImagePathAdmin(admin.ModelAdmin):
	list_display = ('mobile_image_pic', )

admin.site.register(Question)
admin.site.register(MobileInfo, MobileInfoAdmin)
admin.site.register(MobileSpecification, MobileSpecificationAdmin)
admin.site.register(MobileImageLink, MobileImageLinkAdmin)
admin.site.register(MobileImagePath, MobileImagePathAdmin)


