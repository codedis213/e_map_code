from __future__ import unicode_literals

from django.db import models
from datetime import datetime

# Create your models here.


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


class MobileImageLink(models.Model):
	image_link = models.CharField(max_length=256) 

	def __unicode__(self):
		return "%s" %(self.image_link)

class MobileImagePath(models.Model):
    mobile_image_pic = models.ImageField(upload_to = '/static/kooch/img/', verbose_name = "Image Path" )

    def __unicode__(self):
		return "%s" %(self.mobile_image_pic)


class MobileSpecification(models.Model):
	key_name = models.CharField(max_length=50)
	value_info = models.TextField()

	def __unicode__(self):
		return "%s" %(self.key_name)

class MobileInfo(models.Model):
	domain = models.CharField(max_length=50)
	datatype = models.CharField(max_length=50)
	brand = models.CharField(max_length=25, null=True)
	model_name = models.CharField(max_length=40, null=True)
	model_id = models.CharField(max_length=40, null=True)
	title = models.CharField(max_length=100)
	second_title = models.CharField(max_length=100, null=True)
	sub_title =  models.CharField(max_length=100, null=True)
	buy_link = models.CharField(max_length=256)
	rating = models.CharField(max_length=10, null=True)
	price = models.CharField(max_length=10)
	image_list = models.ManyToManyField(MobileImageLink)
	image_path_list = models.ManyToManyField(MobileImagePath)
	specification = models.ManyToManyField(MobileSpecification)
	added = models.DateTimeField(auto_now_add=True, null=True)
	updated = models.DateTimeField(auto_now=True, null=True)

	def __unicode__(self):
		return "%s%s" %(self.title, self.sub_title)