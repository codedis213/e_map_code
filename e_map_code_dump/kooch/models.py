from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

from mongoengine import *
from e_map_code.settings import DBNAME

connect(DBNAME)

class Specification(EmbeddedDocument):
    key_name = StringField()


class ElectronicsItem(Document):
    domain = StringField()
    type = StringField()
    title = StringField()
    sub_title = StringField()
    buy_link = StringField()
    rating = StringField()
    price = StringField()
    image_list = ListField(StringField(max_length=30))
    # specification = ListField(EmbeddedDocumentField(Specification))
    specification = DictField()

    meta = {
        'collection': 'electronics_items_2'
    }