from django.conf.urls import url

from . import views


app_name = 'kooch'

urlpatterns = [
    url(r'^$', views.category, name='category'),
    url(r'^home$', views.index, name='index'),
    url(r'^register$', views.register, name='register'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^category-right$', views.category_right, name='category-right'),
    url(r'^category-full$', views.category_full, name='category-full'),
    url(r'^detaill$', views.detail, name='detail'),
    url(r'^detail/(?P<model_id>\w+)$', views.detail, name='detail2'),
    url(r'^customer-orders$', views.customer_orders, name='customer-orders'),
    url(r'^customer-order$', views.customer_order, name='customer-order'),
    url(r'^customer-wishlist$', views.customer_wishlist, name='customer-wishlist'),
    url(r'^customer-account$', views.customer_account, name='customer-account'),
    url(r'^basket$', views.basket, name='basket'),
    url(r'^checkout1', views.checkout1, name='checkout1'),
    url(r'^checkout2', views.checkout2, name='checkout2'),
    url(r'^checkout3', views.checkout3, name='checkout3'),
    url(r'^checkout4', views.checkout4, name='checkout4'),
    url(r'^blog', views.blog, name='blog'),
    url(r'^post', views.post, name='post'),
    url(r'^faq', views.faq, name='faq'),
    url(r'^text', views.text, name='text'),
    url(r'^text-right', views.text_right, name='text-right'),
    url(r'^_404', views._404, name='_404'),
    url(r'^text', views.text, name='text'),
    url(r'^basket', views.basket, name='basket'),






]