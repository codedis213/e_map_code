#!/usr/bin/python
# -*- coding: ascii -*-
# -*- coding: utf-8 -*-


from PIL import Image
import os 

test_image = '/home/rrvc/ecommerse_practice/e_map_code/e_map_code/kooch/static/kooch/img/banner_demp.jpg'

filename, extention = os.path.splitext(test_image)
outfile1 = filename + "_testing1" + extention
outfile2 = filename + "_testing2" + extention
outfile3 = filename + "_testing3" + extention

size1 = 450, 600
size2 = 450, 678
size3 = 513, 513

img = Image.open(test_image)
img1 = img.resize(size1, Image.ANTIALIAS)
img1.save(outfile1, "JPEG")

img2 = img.resize(size2, Image.ANTIALIAS)
img2.save(outfile2, "JPEG")

img3 = img.resize(size3, Image.ANTIALIAS)
img3.save(outfile3, "JPEG")