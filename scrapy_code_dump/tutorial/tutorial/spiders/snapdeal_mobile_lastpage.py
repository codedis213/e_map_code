import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re

class DmozSpider(scrapy.Spider):
    name = "snapdeal_mobile_lastpage"
    allowed_domains = ["www.snapdeal.com"]
    start_urls = ["http://www.snapdeal.com/product/redmi-2-prime-16-gb/683138068892"]


    def parse(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        item["domain"] = "www.snapdeal.com"
        item["type"] = "Mobile"
        item["title"] = soup.find("h1", attrs={"itemprop":"name"}).get_text().strip()

        try:
            item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        except:
            item["sub_title"] = ''

        item["buy_link"] = response.url

        try:
            item["rating"] = soup.find("span", {"class":"ig-star star-y sd-product-main-rating"}).get("md-data-rating")
        except:
            item["rating"] = ''


        try:
            product_selling_prce = soup.find("span", {"itemprop":"price"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] =''


        try:
            img_list = soup.find("ul", {"id":"bx-slider-left-image-panel"}).find_all("img")

            item["image_list"] = [str(img.get("bigsrc")).strip() for img in img_list]
        except:
            item["image_list"] =''



        specification = {}

        all_table = soup.find("div", attrs={"itemprop":"description"}).find_all("table")

        for table in all_table:
            all_tr = table.find_all("tr")

            header = all_tr[0].find("th").get_text().strip()

            # specification[header] = {}

            for tr in all_tr[1:]:
                try:
                    column = "_".join(tr.find("td").get_text().strip().split()).lower()
                    value = tr.find("td").find_next_sibling("td").get_text().strip()
                    # specification[header][column] = value
                    specification[column] = value
                except:
                    pass


        item["specification"] = specification

        yield item


