import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re
import json

class DmozSpider(scrapy.Spider):
    name = "snapdeal_mobile"
    allowed_domains = ["www.snapdeal.com"]

    start_urls = [
        "http://www.snapdeal.com/products/mobiles-mobile-phones?sort=plrty",
    ]


    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        items_count = soup.find("span", **{"class":"category-count"}).get_text().strip()

        if items_count:
            count_string = "".join([ch for ch in items_count if ch.isdigit()]).strip(".")
            count_string = int(str(count_string))

            href_link_list = ["http://www.snapdeal.com/acors/json/product/get/search/175/"+str(x)+"/48?q=&sort=plrty&brandPageUrl=&keyword=&vc=&webpageName=categoryPage&brandName=&isMC=false&clickSrc="
                             for x in range(48, count_string, 48)]

            # href_link_list = ["http://www.snapdeal.com/acors/json/product/get/search/175/192/48?q=&sort=plrty&brandPageUrl=&keyword=&vc=&webpageName=categoryPage&brandName=&isMC=false&clickSrc="]

            for url in href_link_list:
                yield scrapy.Request(url, callback=self.parse_dir_contents)



    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        a_tag_list = soup2.find_all("div", attrs={"class":"product-desc-rating title-section-expand"})
        product_link_list = [ str(a_tag.find("a").get("href")).strip() for a_tag in a_tag_list]

        # product_link_list = ["http://www.snapdeal.com/product/lg-g2-32-gb-white/1894928895"]
        for url in product_link_list:
                yield scrapy.Request(url, callback=self.parse_pro_link)

    def parse_pro_link(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        item["domain"] = "www.snapdeal.com"
        item["type"] = "Mobile"
        item["title"] = soup.find("h1", attrs={"itemprop":"name"}).get_text().strip()

        try:
            item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        except:
            item["sub_title"] = ''

        item["buy_link"] = response.url

        try:
            item["rating"] = soup.find("span", {"class":"ig-star star-y sd-product-main-rating"}).get("md-data-rating")
        except:
            item["rating"] = ''


        try:
            product_selling_prce = soup.find("span", {"itemprop":"price"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] =''


        try:
            img_list = soup.find("ul", {"id":"bx-slider-left-image-panel"}).find_all("img")

            item["image_list"] = [str(img.get("bigsrc")).strip() for img in img_list]
        except:
            item["image_list"] =''



        specification = {}

        all_table = soup.find("div", attrs={"itemprop":"description"}).find_all("table")

        for table in all_table:
            all_tr = table.find_all("tr")

            header = all_tr[0].find("th").get_text().strip()

            # specification[header] = {}

            for tr in all_tr[1:]:
                try:
                    column = "_".join(tr.find("td").get_text().strip().split()).lower()
                    value = tr.find("td").find_next_sibling("td").get_text().strip()
                    # specification[header][column] = value
                    specification[column] = value
                except:
                    pass


        item["specification"] = specification

        yield item
