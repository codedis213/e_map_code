import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re
import json

class DmozSpider(scrapy.Spider):
    name = "flipkart_mobiles"
    allowed_domains = ["www.flipkart.com"]

    start_urls = [
        "http://www.flipkart.com/mobiles/pr?sid=tyy%2C4io&filterNone=true",
    ]


    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        searchCount = soup.find("div",{"id":"searchCount"})
        items_count = searchCount.find_all("span", **{"class":"items"})

        if items_count:
            count_string = items_count[0].get_text()
            count_string = int(str(count_string.replace(",", "")).strip())

            href_link_list = ["http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=" +str(x)+ "&ajax=true"
                             for x in range(0, count_string, 20)]
            #
            # href_link_list = ['http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?sid=tyy%2C4io&filterNone=true&start=0&ajax=true']

            for url in href_link_list:
                yield scrapy.Request(url, callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        a_tag_list = soup2.find_all("a", {"data-tracking-id":"prd_title"})

        product_link_list = ["http://www.flipkart.com%s" % a_tag.get("href") for a_tag in a_tag_list]

        # product_link_list = [u'http://www.flipkart.com/moto-x-play/p/itmeajtqyhrnkwrz?pid=MOBEAJTQGSBHEFHA&al=%2BbUjNsVCEnprtYCxLB%2FWxcldugMWZuE7Qdj0IGOOVqvmU2ZqdKxAnwpk%2BxH53aGM9Xb7x5fMYT4%3D&ref=L%3A-4668056859870149875&srno=b_40']

        for url in product_link_list:
                yield scrapy.Request(url, callback=self.parse_pro_link)

    def parse_pro_link(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        all_table = soup.find("div", attrs={"class":"productSpecs specSection"}).find_all("table")

        item["domain"] = "www.flipkart.com"
        item["type"] = "Mobile"
        item["title"] = soup.find("h1", attrs={"itemprop":"name"}).get_text().strip()
        item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        item["buy_link"] = response.url
        item["rating"] = ''

        try:
            product_selling_prce = soup.find("span", {"class":"selling-price omniture-field"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] = 'not defined'

        try:
            img_list = soup.find_all("img", attrs={"class":"productImage"})
            item["image_list"] = [img.get("data-zoomimage") for img in img_list]
        except:
            item["image_list"] =''

        specification = {}

        for table in all_table:
            all_tr = table.find_all("tr")

            header = all_tr[0].find("th").get_text().strip()

            # specification[header] = {}

            for tr in all_tr[1:]:
                try:
                    column = "_".join(tr.find("td").get_text().strip().split()).lower()
                    value = tr.find("td").find_next_sibling("td").get_text().strip()
                    # specification[header][column] = value
                    specification[column] = value
                except:
                    pass


        item["specification"] = specification

        yield item