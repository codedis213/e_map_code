import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re

class DmozSpider(scrapy.Spider):
    name = "flipkart_mobile_lastpage"
    allowed_domains = ["www.flipkart.com"]
    start_urls = [
        "http://www.flipkart.com/lenovo-a6000-plus/p/itmeb9agnna2thpa?pid=MOBE6FT8DZXTBRZZ&al=tjzUzlLLok2J12%2BwD85UgMldugMWZuE7Qdj0IGOOVqtmLGDY7tCpjAR29mU%2F4BpoF6okFwRakW0%3D&ref=L%3A4799291936640224319&srno=b_1",
    ]

    def parse(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        all_table = soup.find("div", attrs={"class":"productSpecs specSection"}).find_all("table")

        item["domain"] = "www.flipkart.com"
        item["type"] = "Mobile"
        item["title"] = soup.find("h1", attrs={"itemprop":"name"}).get_text().strip()
        item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        item["buy_link"] = response.url
        item["rating"] = ''

        try:
            product_selling_prce = soup.find("span", {"class":"selling-price omniture-field"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] = 'not defined'

        try:
            img_list = soup.find_all("img", attrs={"class":"productImage"})
            item["image_list"] = [img.get("data-zoomimage") for img in img_list]
        except:
            item["image_list"] =''

        specification = {}

        for table in all_table:
            all_tr = table.find_all("tr")

            header = all_tr[0].find("th").get_text().strip()

            # specification[header] = {}

            for tr in all_tr[1:]:
                try:
                    column = "_".join(tr.find("td").get_text().strip().split()).lower()
                    value = tr.find("td").find_next_sibling("td").get_text().strip()
                    # specification[header][column] = value
                    specification[column] = value
                except:
                    pass


        item["specification"] = specification

        yield item


