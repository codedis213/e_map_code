# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass



class DmozItem(scrapy.Item):
    title = scrapy.Field()
    link = scrapy.Field()
    desc = scrapy.Field()

class ElectronicsItem(scrapy.Item):
    domain = scrapy.Field()
    type = scrapy.Field()
    title = scrapy.Field()
    sub_title = scrapy.Field()
    buy_link = scrapy.Field()
    rating = scrapy.Field()
    price = scrapy.Field()
    image_list = scrapy.Field()
    specification = scrapy.Field()


