import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re
import json
from urlparse import urlparse

class DmozSpider(scrapy.Spider):
    name = "flipkart_mobiles"
    allowed_domains = ["www.flipkart.com"]

    start_urls = [
        "http://www.flipkart.com/mobiles/pr?sid=tyy%2C4io&filterNone=true",
    ]


    def parse(self, response):
        soup0 = BeautifulSoup(response.body, "html.parser")
        brand_ul = soup0.find("ul", attrs={"id":"brand"}).find_all("li")

        for brand_li in brand_ul :
            brand_link = "http://www.flipkart.com/mobiles/pr?p[]=facets.brand[]=Zopo&sid=tyy%2C4io&filterNone=true"
            brand_link = "http://www.flipkart.com/mobiles/pr?p[]=" + brand_li.find("input").get("value").strip() + "&sid=tyy%2C4io&filterNone=true"
            yield scrapy.Request(brand_link, callback=self.parse_brand_link)


    def parse_brand_link(self, response):
        link_split = urlparse(response.url)
        sub_link_one = "http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?"
        sub_link_two = link_split.query
        sub_link_three = "&start=%d&ajax=true"
        brand_link_standard = "%s%s%s" %(sub_link_one, sub_link_two , sub_link_three)

        soup = BeautifulSoup(response.body, "html.parser")
        searchCount = soup.find("div",{"id":"searchCount"})
        items_count = searchCount.find_all("span", **{"class":"items"})

        if items_count:
            count_string = items_count[0].get_text()
            count_string = int(str(count_string.replace(",", "")).strip())

            href_link_list = [ sub_link_one + sub_link_two + sub_link_three %(x) for x in range(20, count_string, 20)]

            if int(count_string % 20):
                count_num = count_string - (count_string % 20)
                link_string = sub_link_one + sub_link_two + sub_link_three %(count_num)
                href_link_list.append(link_string)

            # href_link_list = ['http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList?p%5B%5D=facets.brand%5B%5D=Nokia&sid=tyy%2C4io&filterNone=true&start=180&ajax=true']
            
            for url in href_link_list:
                yield scrapy.Request(url, callback=self.parse_dir_contents)

    
    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        a_tag_list = soup2.find_all("a", {"data-tracking-id":"prd_title"})

        product_link_list = ["http://www.flipkart.com%s" % a_tag.get("href") for a_tag in a_tag_list]

        # product_link_list = ['http://www.flipkart.com/samsung-guru-gt/p/itmebpt3fjyrnpga?pid=MOBEBPT25VG3HGTZ&al=U%2Blvpg%2FZCc%2FJCAZ7RRs8csldugMWZuE7Qdj0IGOOVqvu4ftyQXZYF5quIL%2BdaLrMdc05L09dyos%3D&ref=L%3A-4344932413590648138&srno=b_101']
       
        for url in product_link_list:
                yield scrapy.Request(url, callback=self.parse_pro_link)


    def parse_pro_link(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        all_table = soup.find("div", attrs={"class":"productSpecs specSection"}).find_all("table")

        item["domain"] = "www.flipkart.com"
        item["datatype"] = "Mobile"
        
        try:
            item["brand"] = soup.find("td", text=re.compile("Brand")).find_next("td").get_text().strip()
        except:
            item["brand"] = ""

        
        try:
            item["model_name"] = soup.find("td", text=re.compile("Model Name")).find_next("td").get_text().strip()
        except:
            item["model_name"] = ""

        try:
            item["model_id"] = soup.find("td", text=re.compile("Model ID")).find_next("td").get_text().strip()
        except:
            item["model_id"] = ""

        # item["title"] = soup.find("h1", attrs={"itemprop":"name"}).get_text().strip()
        item["title"] = "%s %s" %(item["brand"], item["model_name"]) 
        item["second_title"] =  "%s %s %s" %(item["brand"], item["model_name"], item["model_id"])

        item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        item["buy_link"] = response.url
        item["rating"] = ''

        try:
            product_selling_prce = soup.find("span", {"class":"selling-price omniture-field"}).get_text()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            item["price"] = 'not defined'

        item["image_list"] = []

        try:
            img_list = soup.find_all("img", attrs={"class":"productImage"})
            item["image_list"] = [img.get("data-zoomimage") for img in img_list]
            item["image_list"] = filter(None, item["image_list"])

            if not item["image_list"]:
                item["image_list"] = [img.get("data-src") for img in img_list]
        except:
            item["image_list"] =[]

        item["image_list"] = filter(None, item["image_list"])

        specification = {}

        for table in all_table:
            all_tr = table.find_all("tr")

            header = all_tr[0].find("th").get_text().strip()

            # specification[header] = {}

            for tr in all_tr[1:]:
                try:
                    # column = "_".join(tr.find("td").get_text().strip().split()).lower()
                    column = tr.find("td").get_text().strip().lower()
                    value = tr.find("td").find_next_sibling("td").get_text().strip()
                    # specification[header][column] = value
                    specification[column] = value
                except:
                    pass


        item["specification"] = specification

        yield item