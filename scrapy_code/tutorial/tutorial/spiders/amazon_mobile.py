import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re
import json
import ast
from urlparse import urlparse

class DmozSpider(scrapy.Spider):
    name = "amazon_mobile"
    allowed_domains = ["www.amazon.com", "www.amazon.in"]

    start_urls = [
        "http://www.amazon.in/s/ref=sr_st_price-desc-rank?rh=n%3A976419031%2Cn%3A!976420031%2Cn%3A1389401031%2Cn%3A1389432031&qid=1458965324&sort=price-desc-rank",
    ]


    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")

        h2_count = soup.find("h2", attrs={"id":"s-result-count"}).get_text()
        # h2_count = "u'1-24 of 2,899 results for Electronics : Mobiles & Accessories : Smartphones & Basic Mobiles'"
        
        of_pos = h2_count.find("of")
        result_pos  = h2_count.find("result")
        
        mobile_count = h2_count[of_pos+2:result_pos].strip()
        mobile_count = int("".join([ch for ch in mobile_count if ch.isdigit()]))

        mobile_range = mobile_count / 24 

        if not (mobile_count % 24):
            mobile_range += 1

        # link = "http://www.amazon.in/s/ref=sr_pg_3?rh=n%3A976419031%2Cn%3A!976420031%2Cn%3A1389401031%2Cn%3A1389432031&page=3&sort=price-desc-rank&ie=UTF8&qid=1459010543"

        link_part_1 = "http://www.amazon.in/s/ref=sr_pg_"
        link_part_2 = "?rh=n%3A976419031%2Cn%3A!976420031%2Cn%3A1389401031%2Cn%3A1389432031&page="
        link_part3 = "&sort=price-desc-rank&ie=UTF8&qid=1459010543"

        for i in range(1, mobile_range+1):
            url = link_part_1 + str(i) + link_part_2 + str(i) + link_part3
            yield scrapy.Request(url, callback=self.parse_dir_contents)

    
    def parse_dir_contents(self, response):
        soup2 = BeautifulSoup(response.body, "html.parser")
        all_li = soup2.find_all("li", attrs={"class":"s-result-item  celwidget "}) 
        
        for li in all_li:
            url = li.find("a", attrs={"class":"a-link-normal a-text-normal"}).get("href")
            yield scrapy.Request(url, callback=self.parse_pro_link)

    
    def parse_pro_link(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        item["domain"] = "www.amazon.com"
        item["datatype"] = "Mobile"

        item["brand"] = soup.find("a", attrs={"id":"brand"}).get_text()
                
        try:
            item["model_id"] = soup.find("td", text=re.compile("Model ID")).find_next_sibling().get_text()
        except:
            item["model_id"] = ''
        
        item["title"] = soup.find("span", attrs={"id":"productTitle"}).get_text()
        

        try:
            item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        except:
            item["sub_title"] = ''

        item["buy_link"] = response.url

        try:
            star_string = soup.find("div", attrs={"id":"averageCustomerReviews"}).find("span", attrs={"class":"a-icon-alt"}).get_text()
            item["rating"] = float(star_string[:star_string.find("out")].strip())
        except:
            item["rating"] = ''


        try:
            product_selling_prce = soup.find("div", attrs={"id":"price"}).find("span", attrs={"class":"a-color-price"}).get_text().strip()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            try:
                product_selling_prce = soup.find("span", attrs={"class":"a-color-price"}).get_text().strip()
                item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
            except:
                item["price"] =''

        try:
            image_size_list = soup.find("div", attrs={"id":"imgTagWrapperId"}).find("img").get("data-a-dynamic-image")
            image_size_list = ast.literal_eval(image_size_list)
            image_size_list = image_size_list.values()

            all_img_li= soup.find("div", attrs={"id":"imageBlock"}).find_all("li", attrs={"class":"a-spacing-small item"})

            item["image_list"] = []

            for img_li in all_img_li:
                img_link = img_li.find("img").get("src")
                parsed = urlparse(img_link)
                dot_point = parsed.path.find("._")
                img_link = parsed.scheme + "://" + parsed.netloc + parsed.path[:dot_point] 
                img_link += "._SY"+ str(image_size_list[0][1]) +"_.jpg"
                item["image_list"].append(img_link)
        except:
            item["image_list"] =''


        specification = {}

        item["model_name"] = ''

        all_spec_li = soup.find("div", attrs={"class":"section techD"}).find_all("td", attrs={"class":"label"})

        for spec_li in all_spec_li:
            key = spec_li.get_text().strip().lower()
            value = spec_li.find_next("td").get_text().lower()
            
            if key == "item model number":
                key = "model name"
                item["model_name"] = value
            
            specification[key] = value


        item["specification"] = specification

        item["second_title"] =  "%s %s %s" %(item["brand"], item["model_name"], item["model_id"])
        item["second_title"] = item["second_title"].strip()
        

        yield item