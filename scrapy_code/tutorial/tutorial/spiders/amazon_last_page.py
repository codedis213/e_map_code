import scrapy

from tutorial.items import DmozItem, ElectronicsItem
from bs4 import BeautifulSoup
import re
import ast
from urlparse import urlparse

class DmozSpider(scrapy.Spider):
    name = "amazon_mobile_lastpage"
    allowed_domains = ["www.amazon.com", "www.amazon.in"]
    start_urls = ["http://www.amazon.in/Apple-iPhone-6s-Plus-Silver/dp/B016QBV406/"]


    def parse(self, response):
        item = ElectronicsItem()
        page = response.body
        soup = BeautifulSoup(page, "html.parser")

        item["domain"] = "www.amazon.com"
        item["datatype"] = "Mobile"

        item["brand"] = soup.find("a", attrs={"id":"brand"}).get_text()
                
        try:
            item["model_id"] = soup.find("td", text=re.compile("Model ID")).find_next_sibling().get_text()
        except:
            item["model_id"] = ''
        
        item["title"] = soup.find("span", attrs={"id":"productTitle"}).get_text()
        

        try:
            item["sub_title"] = soup.find("span", attrs={"class":"subtitle"}).get_text().strip()
        except:
            item["sub_title"] = ''

        item["buy_link"] = response.url

        try:
            star_string = soup.find("div", attrs={"id":"averageCustomerReviews"}).find("span", attrs={"class":"a-icon-alt"}).get_text()
            item["rating"] = float(star_string[:star_string.find("out")].strip())
        except:
            item["rating"] = ''


        try:
            product_selling_prce = soup.find("div", attrs={"id":"price"}).find("span", attrs={"class":"a-color-price"}).get_text().strip()
            item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
        except:
            try:
                product_selling_prce = soup.find("span", attrs={"class":"a-color-price"}).get_text().strip()
                item["price"] = "".join([ch for ch in product_selling_prce if ch.isdigit() or ch=="."]).strip(".")
            except:
                item["price"] =''

        try:
            image_size_list = soup.find("div", attrs={"id":"imgTagWrapperId"}).find("img").get("data-a-dynamic-image")
            image_size_list = ast.literal_eval(image_size_list)
            image_size_list = image_size_list.values()

            all_img_li= soup.find("div", attrs={"id":"imageBlock"}).find_all("li", attrs={"class":"a-spacing-small item"})

            item["image_list"] = []

            for img_li in all_img_li:
                img_link = img_li.find("img").get("src")
                parsed = urlparse(img_link)
                dot_point = parsed.path.find("._")
                img_link = parsed.scheme + "://" + parsed.netloc + parsed.path[:dot_point] 
                img_link += "._SY"+ str(image_size_list[0][1]) +"_.jpg"
                item["image_list"].append(img_link)
        except:
            item["image_list"] =''


        specification = {}

        item["model_name"] = ''

        all_spec_li = soup.find("div", attrs={"class":"section techD"}).find_all("td", attrs={"class":"label"})

        for spec_li in all_spec_li:
            key = spec_li.get_text().strip().lower()
            value = spec_li.find_next("td").get_text().lower()
            
            if key == "item model number":
                key = "model name"
                item["model_name"] = value
            
            specification[key] = value


        item["specification"] = specification

        item["second_title"] =  "%s %s %s" %(item["brand"], item["model_name"], item["model_id"])
        item["second_title"] = item["second_title"].strip()
        

        yield item