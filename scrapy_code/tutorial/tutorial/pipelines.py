# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


from kooch.models import *
import functools 
import sys
import os 


img_directory = "/home/rrvc/ecommerse_practice/e_map_code/e_map_code/kooch/static/kooch/img"
image_upload_path = "/static/kooch/img"
sys.path.append(img_directory)



# def check_spider_pipeline(process_item_method):
#     """
#         This wrapper makes it so pipelines can be turned on and off at a spider level.
#     """
#     @functools.wraps(process_item_method)
#     def wrapper(self, item, spider):
#         if self.__class__ in spider.pipeline:
#             return process_item_method(self, item, spider)
#         else:
#             return item

#     return wrapper


 
class ElectronicsPipeline(object):

    # @check_spider_pipeline
    def process_item(self, item, spider):

        mobj, created = MobileInfo.objects.get_or_create(domain=item["domain"], datatype=item["datatype"], 
            title=item["title"])
        mobj.brand = item["brand"]
        mobj.model_name = item["model_name"]
        mobj.model_id = item["model_id"]
        mobj.second_title = item["second_title"]
        mobj.sub_title=item["sub_title"]
        mobj.buy_link=item["buy_link"]
        mobj.rating=item["rating"]
        mobj.price = item["price"]
        mobj.save()

        image_obj_list = [MobileImageLink.objects.get_or_create(image_link=image_link)[0] 
        for image_link in item["image_list"]]

        if mobj.image_list:
            mobj.image_list.remove(*image_obj_list)
            print "deleted image_list"

        mobj.image_list.add(*image_obj_list)

        spec_obj_list = [MobileSpecification.objects.get_or_create(key_name=key_name, value_info=value_info)[0]
        for key_name, value_info in item["specification"].iteritems()]
        
        if mobj.specification:
            mobj.specification.remove(*spec_obj_list)
            print "deleted specification"

        mobj.specification.add(*spec_obj_list)

        return item


# class TutorialPipeline(object):
#     def process_item(self, item, spider):
#         return item


# import pymongo

# class MongoPipeline(object):

#     collection_name = 'scrapy_items'

#     def __init__(self, mongo_uri, mongo_db):
#         self.mongo_uri = mongo_uri
#         self.mongo_db = mongo_db

#     @classmethod
#     def from_crawler(cls, crawler):
#         return cls(
#             mongo_uri=crawler.settings.get('MONGO_URI'),
#             mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
#         )

#     def open_spider(self, spider):
#         self.client = pymongo.MongoClient(self.mongo_uri)
#         self.db = self.client[self.mongo_db]

#     def close_spider(self, spider):
#         self.client.close()

#     def process_item(self, item, spider):
#         self.db[self.collection_name].insert(dict(item))
#         return item


# class MongoPipelineElectronics(object):

#     collection_name = 'electronics_items_2'

#     def __init__(self, mongo_uri, mongo_db):
#         self.mongo_uri = mongo_uri
#         self.mongo_db = mongo_db

#     @classmethod
#     def from_crawler(cls, crawler):
#         return cls(
#             mongo_uri=crawler.settings.get('MONGO_URI'),
#             mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
#         )

#     def open_spider(self, spider):
#         self.client = pymongo.MongoClient(self.mongo_uri)
#         self.db = self.client[self.mongo_db]

#     def close_spider(self, spider):
#         self.client.close()

#     def process_item(self, item, spider):
#         self.db[self.collection_name].insert(dict(item))
#         return item
