# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from kooch.models import MobileInfo, MobileImageLink, MobileSpecification

class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass



class DmozItem(scrapy.Item):
    title = scrapy.Field()
    link = scrapy.Field()
    desc = scrapy.Field()


# class ElectronicsImageLink(DjangoItem):
    # django_model = MobileImageLink

# class ElectronicsSpecification(DjangoItem):
    # django_model = MobileSpecification



class ElectronicsItem(DjangoItem):
    domain = scrapy.Field()
    datatype = scrapy.Field()
    brand = scrapy.Field()
    model_name = scrapy.Field()
    model_id = scrapy.Field()
    title = scrapy.Field()
    second_title = scrapy.Field()
    sub_title = scrapy.Field()
    buy_link = scrapy.Field()
    rating = scrapy.Field()
    price = scrapy.Field()
    image_list = scrapy.Field()
    specification = scrapy.Field()
    # django_model = MobileInfo


