import base64
import logging
from random import choice


class OpenProxyFile(object):

    def __init__(self):
        self.directory_path = "/home/rrvc/ecommerse_practice/e_map_code/scrapy_code/tutorial/tutorial"
        self.filename = "%s/proxies.txt" % self.directory_path
        with open(self.filename) as f:
            self.list_of_proxies = f.readlines()


class ProxyMiddleware(OpenProxyFile):

    def __init__(self):
        OpenProxyFile.__init__(self)

    def process_request(self, request, spider):
        proxy_str = choice(self.list_of_proxies)
        request.meta['proxy'] = "http://%s:80" % proxy_str.strip()
        logging.debug("http://%s:80" % proxy_str.strip())
