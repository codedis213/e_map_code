# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class MobileImageDownloaderPipeline(object):
    def process_item(self, item, spider):
        return item


import scrapy
from scrapy.pipelines.images import ImagesPipeline

from kooch.models import *
import functools 
import sys
import os 


img_directory = "/home/rrvc/ecommerse_practice/e_map_code/e_map_code/kooch/static/kooch/img"
image_upload_path = "/static/kooch/img"
sys.path.append(img_directory)


class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        for image_url in item['image_urls']:
            yield scrapy.Request(image_url)

    def item_completed(self, results, item, info):

        # print results
        
        # results = [(True, {'url': 'http://img5a.flixcart.com/image/mobile/r/z/z/lenovo-a6000-plus-a6000-plus-1100x1100-imae6fyhhrwnaywc.jpeg', 
        #     'path': 'full/445c73c37637547b4771646d008e739d1d1834f1.jpg', 
        #     'checksum': '798d309f1cf73765ef52d35dfaf7bbf9'}), 
        # (True, {'url': 'http://img6a.flixcart.com/image/mobile/r/z/z/lenovo-a6000-plus-p0sb004bin-p0sb004ein-p0sb0010in-1100x1100-imaeg888mgaqtmvq.jpeg', 
        #     'path': 'full/b21c7a9b9e165fd1870b257134cc6349b3b956e7.jpg', 
        #     'checksum': '165c4acee8adc6fe37e574775cb0c522'})]

        for ok, x in results:
            if ok:

                mlobj_list = MobileImageLink.objects.filter(image_link = x['url']).all()
                
                for mlobj in mlobj_list:
                    
                    minfobj_list = mlobj.mobileinfo_set.all()
                    imageobj_list = []

                    for minfobj in minfobj_list:
                    
                        img_hash = x["path"].split("/")[-1]

                        fname = "%s/full/%s" %(img_directory, img_hash)
                    
                        if os.path.isfile(fname):
                            image_pth = "%s/full/%s" %(image_upload_path, img_hash) 
                            mipath = MobileImagePath.objects.get_or_create(mobile_image_pic=image_pth)[0]
                            imageobj_list.append(mipath)

                        fname = "%s/thumbs/big/%s" %(img_directory, img_hash )
                    
                        if os.path.isfile(fname):
                            image_pth = "%s/thumbs/big/%s" %(image_upload_path, img_hash) 
                            mipath = MobileImagePath.objects.get_or_create(mobile_image_pic=image_pth)[0]
                            imageobj_list.append(mipath)


                        fname = "%s/thumbs/medium/%s" %(img_directory, img_hash )
                    
                        if os.path.isfile(fname):
                            image_pth = "%s/thumbs/medium/%s" %(image_upload_path, img_hash) 
                            mipath = MobileImagePath.objects.get_or_create(mobile_image_pic=image_pth)[0]
                            imageobj_list.append(mipath)


                        fname = "%s/thumbs/small/%s" %(img_directory, img_hash )

                        if os.path.isfile(fname):
                            image_pth = "%s/thumbs/small/%s" %(image_upload_path, img_hash) 
                            mipath = MobileImagePath.objects.get_or_create(mobile_image_pic=image_pth)[0]
                            imageobj_list.append(mipath)

                    # print imageobj_list

                    if imageobj_list:
                        # MobileImagePath.objects.bulk_create(imageobj_list)
                        print img_directory
                        minfobj.image_path_list = imageobj_list
                        minfobj.save()


        image_paths = [x['url'] for ok, x in results if ok]

        if not image_paths:
            raise DropItem("Item contains no images")
        item['image_paths'] = image_paths
        return item
