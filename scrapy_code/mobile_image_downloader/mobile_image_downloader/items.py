# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from kooch.models import MobileInfo, MobileImageLink, MobileSpecification



class MobileImageDownloaderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class MyItem(scrapy.Item):
    # ... other item fields ...
    image_urls = scrapy.Field()
    image_paths = scrapy.Field()
