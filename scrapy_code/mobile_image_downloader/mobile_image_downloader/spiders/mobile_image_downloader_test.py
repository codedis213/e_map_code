import scrapy
from scrapy.pipelines.images import ImagesPipeline
from mobile_image_downloader.items import MobileImageDownloaderItem, MyItem
from bs4 import BeautifulSoup
import re
from django.db import connection
from kooch.models import MobileInfo



class DmozSpider(scrapy.Spider):
    name = "mobile_image_downloader_test"

    pipeline = set([
        'pipelines.MyImagesPipeline'
            ])

    cursor = connection.cursor()
    cursor.execute("select distinct domain from kooch_mobileinfo")

    data = cursor.fetchall()
    
    allowed_domains = [d[0] for d in data]

    cursor.close()

    start_urls = [u'http://img5a.flixcart.com/image/mobile/r/z/z/lenovo-a6000-plus-a6000-plus-1100x1100-imae6fyhhrwnaywc.jpeg'] 


    
    def parse(self, response):

        mobj_list = MobileInfo.objects.filter(image_path_list__mobile_image_pic__isnull = True)
        print mobj_list.__len__()

        start_urls = [str(link["image_link"]) for mobj in  mobj_list[:10] for link in mobj.image_list.values("image_link")] 

        # start_urls2 = [u'http://img5a.flixcart.com/image/mobile/r/z/z/lenovo-a6000-plus-a6000-plus-1100x1100-imae6fyhhrwnaywc.jpeg', 
        #     u'http://img6a.flixcart.com/image/mobile/r/z/z/lenovo-a6000-plus-p0sb004bin-p0sb004ein-p0sb0010in-1100x1100-imaeg888mgaqtmvq.jpeg']

        yield MyItem(
            image_urls = start_urls
        )     

    